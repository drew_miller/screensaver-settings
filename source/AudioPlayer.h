#ifndef AUDIOPLAYER_H_
#define AUDIOPLAYER_H_

#include <pulse/error.h>
#include <pulse/simple.h>

#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QWaitCondition>

#define SAMPLERATE 44100
#define BUFSIZE (SAMPLERATE/120)
#define NUMFREQUENCIES 3
#define PI 3.14159265358979323851280895940618620443274267017841339111328125

class AudioPlayer: public QObject {
Q_OBJECT

public:
	explicit AudioPlayer(QObject *parent = NULL);
	virtual ~AudioPlayer();

public slots:
	void run();
	bool pause();
	void pauseAsync();
	bool unpause();
	void unpauseAsync();
	bool stop();
	void stopAsync();
	bool isPlaying();
	bool isPaused();
	bool isStopped();
	double getVolume();
	void setVolume(double volume);

private:
	pa_simple *s;
	QMutex* mutex;
	QWaitCondition* currStateChangedCondition;
	QWaitCondition* nextStateChangedCondition;
	double currVolume, nextVolume;
	enum PlayerState {
		playing, paused, stopped
	} currState, nextState;

};

#endif /* AUDIOPLAYER_H_ */
