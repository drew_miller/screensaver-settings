= Screensaver Settings =


== About ==
Screensaver and Audio Settings
• Enable/disable pause-on-screensaver
• Enable/disable mute-on-screensaver
• Set audio output sound level required to inhibit screensaver.
• Determine whether the PausumeService should show notifications, and how long they should last.

Configuration tool for the Pausume Service.

== System Requirements ==
You need to have the following libraries installed:
 * pausumeservice. If you have this installed, then you also have already met 
       the following dependencies.
 * libqt5dbus5. To communicate between the frontend configuration and the 
       PausumeService D-Bus service.
 * libqt5widgets5. Because screensaver-settings is a windowed application with 
       checkboxes and stuff.
 * libnotify. Preferably a version with "actions" capability such as the XFCE 
       notification daemon or Gnome Shell's. Ubuntu's notification doesn't have 
       good support for this feature.

== Building and running ==

=== Debug ===
mkdir debug
cd debug
cmake ../source -DDEBUG_BUILD=1 # Complains that the define isn't used, but it is.
make
./screensaver-settings # prints extra debugging information.

=== Release ===
mkdir release
cd release
cmake ../source
make
./screensaver-settings

=== Package ===
sudo apt-get install cpack # Used for creating packages from cmake.
sudo apt-get install rpm # Needed for rpmbuild, generating RPM package.
sudo apt-get install libpulse-dev # PulseAudio headers
sudo apt-get install qt5-qmake # Not sure if there is a smaller package that would also provide this...
mkdir package
cd package
cmake ../source
sudo cpack # Generates DEB, RPM, and TAR packages. Also installs program, which is kind of annoying.
screensaver-settings


== License ==
This software is licensed under the GPLv3.
https://www.gnu.org/licenses/gpl-3.0.txt


== Resources ==
 * Qt5 class reference:
 	https://qt-project.org/doc/qt-5.0/qtdoc/classes.html
 * Sample graphical Qt application:
 	http://doc.qt.digia.com/qq/qq25-formlayout.html
 	http://www.developer.nokia.com/info/sw.nokia.com/id/e775b575-89be-44d5-ab48-738be0fb2100/Qt_QTabs_Example_v1_1_en.zip.html
 * Accessing DBus interfaces:
 	http://techbase.kde.org/Development/Tutorials/D-Bus/Accessing_Interfaces
 * Where are .desktop application files stored?
 	http://askubuntu.com/questions/217331/where-are-the-desktop-icon-files-stored


// Drew Miller
dmiller309@gmail.com
