#include "ScreensaverSettings.h"

#include <iostream>

#include <QtCore/QTimer>

#include <QtGui/QIcon>

#include <QtWidgets/QApplication>

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	app.setOrganizationName("Craven Company");
	app.setApplicationName("Screensaver Settings");
	ScreensaverSettings w;
	app.setWindowIcon(QIcon("/usr/share/icons/hicolor/256x256/apps/screensaver-control256.png"));
	w.showNormal();
	return app.exec();
}

