#ifndef SCREENSAVERSETTINGS_H_
#define SCREENSAVERSETTINGS_H_

#include "AudioPlayer.h"

#include <QtDBus/QDBusInterface>

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>

#define VOLUME_THRESHOLD_SLIDER_MAX 100

#define VOLUME_DURATION 2000

class ScreensaverSettings: public QMainWindow {
Q_OBJECT

public:
ScreensaverSettings(QWidget *parent = 0);
	virtual ~ScreensaverSettings();

public slots:
	bool getPauseForScreensaver();
	void setPauseForScreensaver(bool doPause);
	bool getResumeAfterScreensaver();
	void setResumeAfterScreensaver(bool doResume);
	bool getMuteForScreensaver();
	void setMuteForScreensaver(bool doMute);
	bool getUnmuteAfterScreensaver();
	void setUnmuteAfterScreensaver(bool doUnmute);
	bool getPlaybackInhibitsScreensaver();
	void setPlaybackInhibitsScreensaver(bool playbackInhibitsScreensaver);
	bool getAudioInhibitsScreensaver();
	void setAudioInhibitsScreensaver(bool audioInhibitsScreensaver);
	double getVolumeThreshold();
	void setVolumeThreshold(double volumeThreshold);
	bool getNotificationsEnabled();
	void setNotificationsEnabled(bool notificationsEnabled);
	int getNotificationDuration();
	void setNotificationDuration(int duration);
	void restoreDefaults();

private slots:
	void handlePropertyChanged(QString property);
	void handlePauseMusicCheckBoxStateChange(int state);
	void handleResumeMusicCheckBoxStateChange(int state);
	void handleMuteVolumeCheckBoxStateChanged(int state);
	void handleUnmuteVolumeCheckboxStateChanged(int state);
	void handlePlaybackInhibitsCheckBoxStateChanged(int state);
	void handleAudioInhibitsCheckBoxStateChange(int state);
	void handleVolumeThresholdSliderValueChanged(int state);
	void handleNotificationEnabledCheckboxStateChanged(int state);
	void handleNotificationDurationSpinBoxValueChanged(int state);
	void handleRestoreDefaultsButtonClicked(bool);
	void handleVolumeSliderPressed();
	void handleVolumeSliderReleased();
	void checkUnpauseAsync();

private:
	QDBusInterface* screensaverControl;
	QDBusInterface* screensaverControl_properties;
	QCheckBox *pauseMusicCheckBox;
	QCheckBox *resumeMusicCheckBox;
	QCheckBox *muteVolumeCheckBox;
	QCheckBox *unmuteVolumeCheckBox;
	QCheckBox *playbackInhibitsCheckBox;
	QCheckBox *audioInhibitsCheckBox;
	QSlider *volumeThresholdSlider;
	QCheckBox *notificationsEnabledCheckbox;
	QSpinBox *notificationDurationSpinBox;
	QDialogButtonBox *buttonBox;
	QWidget *centralWidget;
	AudioPlayer* audioPlayer;
	QThread* audioPlayer_thread;
	bool volumeSliderPressed;
};

#endif /* SCREENSAVERSETTINGS_H_ */
