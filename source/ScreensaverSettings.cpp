#include "ScreensaverSettings.h"
#include "AudioPlayer.h"

#include <screensaver-control/Config.h>

#include <QtCore/QTimer>
#include <QtCore/QThread>

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusReply>
#include <QtDBus/QDBusVariant>

#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>

#include "unistd.h"

ScreensaverSettings::ScreensaverSettings(QWidget *parent) :
		QMainWindow(parent) {
	screensaverControl = new QDBusInterface(SCREENSAVER_SERVICE,
	SCREENSAVER_OBJECT, SCREENSAVER_INTERFACE, QDBusConnection::sessionBus(),
			this);
	screensaverControl_properties = new QDBusInterface(SCREENSAVER_SERVICE,
			SCREENSAVER_OBJECT, "org.freedesktop.DBus.Properties",
			QDBusConnection::sessionBus(), this);

	setWindowTitle("Screensaver Settings");

	centralWidget = new QWidget(this);
	QFormLayout *mainLayout = new QFormLayout(centralWidget);

	// Create the settings
	pauseMusicCheckBox = new QCheckBox("&Pause for screensaver", centralWidget);
	resumeMusicCheckBox = new QCheckBox("&Resume after screensaver",
			centralWidget);
	muteVolumeCheckBox = new QCheckBox("&Mute for screensaver", centralWidget);
	unmuteVolumeCheckBox = new QCheckBox("&Unmute after screensaver",
			centralWidget);
	playbackInhibitsCheckBox = new QCheckBox(
			"M&edia players inhibit screensaver", centralWidget);
	audioInhibitsCheckBox = new QCheckBox("&Audio output inhibits screensaver",
			centralWidget);
	volumeThresholdSlider = new QSlider(Qt::Orientation::Horizontal,
			centralWidget);
	notificationsEnabledCheckbox = new QCheckBox("&Notifications enabled",
			centralWidget);
	notificationDurationSpinBox = new QSpinBox(centralWidget);

	// Audio player for volume slider
	volumeSliderPressed = false;
	audioPlayer_thread = new QThread();
	audioPlayer = new AudioPlayer();
	audioPlayer->moveToThread(audioPlayer_thread);
	if (getAudioInhibitsScreensaver()) {
		connect(audioPlayer_thread, SIGNAL(started()), audioPlayer,
		SLOT(run()));
		connect(audioPlayer_thread, SIGNAL(finished()), audioPlayer,
		SLOT(deleteLater()));
	}
	audioPlayer_thread->start();

	QDBusConnection::sessionBus().connect("com.ScreensaverControl",
			"/com/ScreensaverControl", "com.ScreensaverControl",
			"propertiesChanged", this,
			SLOT(handlePropertyChanged(QString)));

	// Load the setting values
	pauseMusicCheckBox->setChecked(getPauseForScreensaver());
	resumeMusicCheckBox->setChecked(getResumeAfterScreensaver());
	muteVolumeCheckBox->setChecked(getMuteForScreensaver());
	unmuteVolumeCheckBox->setChecked(getUnmuteAfterScreensaver());
	playbackInhibitsCheckBox->setChecked(getPlaybackInhibitsScreensaver());
	audioInhibitsCheckBox->setChecked(getAudioInhibitsScreensaver());
	volumeThresholdSlider->setValue(
			expm1(getVolumeThreshold()) / NUMFREQUENCIES
					* VOLUME_THRESHOLD_SLIDER_MAX);
	notificationsEnabledCheckbox->setChecked(getNotificationsEnabled());
	notificationDurationSpinBox->setMinimum(0);
	notificationDurationSpinBox->setMaximum(30);
	notificationDurationSpinBox->setValue(getNotificationDuration() / 1000);

	// Handle user interaction
	connect(pauseMusicCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handlePauseMusicCheckBoxStateChange(int)));
	connect(resumeMusicCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handleResumeMusicCheckBoxStateChange(int)));
	connect(muteVolumeCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handleMuteVolumeCheckBoxStateChanged(int)));
	connect(unmuteVolumeCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handleUnmuteVolumeCheckboxStateChanged(int)));
	connect(playbackInhibitsCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handlePlaybackInhibitsCheckBoxStateChanged(int)));
	connect(audioInhibitsCheckBox, SIGNAL(stateChanged(int)), this,
	SLOT(handleAudioInhibitsCheckBoxStateChange(int)));
	connect(volumeThresholdSlider, SIGNAL(valueChanged(int)),
	SLOT(handleVolumeThresholdSliderValueChanged(int)));
	connect(notificationsEnabledCheckbox, SIGNAL(stateChanged(int)), this,
	SLOT(handleNotificationEnabledCheckboxStateChanged(int)));
	connect(notificationDurationSpinBox, SIGNAL(valueChanged(int)), this,
	SLOT(handleNotificationDurationSpinBoxValueChanged(int)));

	// Connect signals to play threshold volume for volume slider
	connect(volumeThresholdSlider, SIGNAL(sliderPressed()), this,
	SLOT(handleVolumeSliderPressed()));
	connect(volumeThresholdSlider, SIGNAL(sliderReleased()), this,
	SLOT(handleVolumeSliderReleased()));

	// Build the form
	// Music and Video section
	QGroupBox *musicGroupBox = new QGroupBox("Music and Video", centralWidget);
	QVBoxLayout *musicBoxLayout = new QVBoxLayout(musicGroupBox);
	musicBoxLayout->addWidget(pauseMusicCheckBox);
	musicBoxLayout->addWidget(resumeMusicCheckBox);
	mainLayout->addRow(musicGroupBox);
	// Volume section
	QGroupBox *volumeGroupBox = new QGroupBox("Volume", centralWidget);
	QVBoxLayout *volumeBoxLayout = new QVBoxLayout(volumeGroupBox);
	volumeBoxLayout->addWidget(muteVolumeCheckBox);
	volumeBoxLayout->addWidget(unmuteVolumeCheckBox);
	mainLayout->addRow(volumeGroupBox);
	// Audio Output section
	QGroupBox *audioOutGroupBox = new QGroupBox("Audio Output", centralWidget);
	QGridLayout *audioOutGridLayout = new QGridLayout(audioOutGroupBox);
	audioOutGridLayout->addWidget(playbackInhibitsCheckBox, 0, 0, 1, 2);
	QLabel* volumeThresholdLabel = new QLabel("Volume threshold:",
			centralWidget);
	audioOutGridLayout->addWidget(audioInhibitsCheckBox, 1, 0, 1, 2);
	audioOutGridLayout->addWidget(volumeThresholdLabel, 2, 0);
	audioOutGridLayout->addWidget(volumeThresholdSlider, 2, 1);
	volumeThresholdSlider->setMinimum(0);
	volumeThresholdSlider->setMaximum(VOLUME_THRESHOLD_SLIDER_MAX);
	volumeThresholdSlider->setValue(
			expm1(
					getVolumeThreshold()) * NUMFREQUENCIES * VOLUME_THRESHOLD_SLIDER_MAX);
	mainLayout->addRow(audioOutGroupBox);
	// Notifications section
	QGroupBox *notificationGroupBox = new QGroupBox("Notifications",
			centralWidget);
	QGridLayout *notificationsBoxLayout = new QGridLayout(notificationGroupBox);
	QLabel* notificationDurationLabel = new QLabel("Duration (seconds):",
			centralWidget);
	notificationsBoxLayout->addWidget(notificationsEnabledCheckbox, 0, 0, 1, 2);
	notificationsBoxLayout->addWidget(notificationDurationLabel, 1, 0);
	notificationsBoxLayout->addWidget(notificationDurationSpinBox, 1, 1);
	mainLayout->addRow(notificationGroupBox);

	buttonBox = new QDialogButtonBox(this);
	QPushButton* restoreDefaultsButton = buttonBox->addButton(
			QDialogButtonBox::StandardButton::RestoreDefaults);
	connect(restoreDefaultsButton, SIGNAL(clicked(bool)), this,
	SLOT(handleRestoreDefaultsButtonClicked(bool)));
	buttonBox->addButton(QDialogButtonBox::StandardButton::Close);
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(close()));
	mainLayout->addWidget(buttonBox);

	centralWidget->setLayout(mainLayout);
	setCentralWidget(centralWidget);
}

ScreensaverSettings::~ScreensaverSettings() {
	audioPlayer->stop();
	delete audioPlayer;
	audioPlayer_thread->deleteLater();
}

void ScreensaverSettings::handlePropertyChanged(QString property) {
	// Properties
	if (property == "properties_version") {
		// Not displayed
	} else if (property == "pause_for_screensaver") {
		pauseMusicCheckBox->setChecked(getPauseForScreensaver());
	} else if (property == "resume_after_screensaver") {
		resumeMusicCheckBox->setChecked(getResumeAfterScreensaver());
	} else if (property == "mute_for_screensaver") {
		muteVolumeCheckBox->setChecked(getMuteForScreensaver());
	} else if (property == "unmute_after_screensaver") {
		unmuteVolumeCheckBox->setChecked(getUnmuteAfterScreensaver());
	} else if (property == "playback_inhibits_screensaver") {
		playbackInhibitsCheckBox->setChecked(getPlaybackInhibitsScreensaver());
	} else if (property == "audio_inhibits_screensaver") {
		audioInhibitsCheckBox->setChecked(getAudioInhibitsScreensaver());
	} else if (property == "audio_sleep_inhibit_power") {
		volumeThresholdSlider->setValue(
				expm1(getVolumeThreshold()) * VOLUME_THRESHOLD_SLIDER_MAX
				* NUMFREQUENCIES);
	} else if (property == "audio_observation_period") {
		// Not displayed.
	} else if (property == "notifications_enabled") {
		notificationsEnabledCheckbox->setChecked(getNotificationsEnabled());
	} else if (property == "notification_duration") {
		// Divide by 1000 to go from milliseconds to seconds.
		notificationDurationSpinBox->setValue(getNotificationDuration() / 1000);
	}
}

bool ScreensaverSettings::getPauseForScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"pause_for_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setPauseForScreensaver(bool doPause) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "pause_for_screensaver",
			QVariant::fromValue(QDBusVariant(doPause)));
}

bool ScreensaverSettings::getResumeAfterScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"resume_after_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setResumeAfterScreensaver(bool doResume) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "resume_after_screensaver",
			QVariant::fromValue(QDBusVariant(doResume)));
}

bool ScreensaverSettings::getMuteForScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"mute_for_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setMuteForScreensaver(bool doMute) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "mute_for_screensaver",
			QVariant::fromValue(QDBusVariant(doMute)));
}

bool ScreensaverSettings::getUnmuteAfterScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"unmute_after_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setUnmuteAfterScreensaver(bool doUnmute) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "unmute_after_screensaver",
			QVariant::fromValue(QDBusVariant(doUnmute)));
}

bool ScreensaverSettings::getPlaybackInhibitsScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"playback_inhibits_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setPlaybackInhibitsScreensaver(
		bool playbackInhibitsScreensaver) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "playback_inhibits_screensaver",
			QVariant::fromValue(QDBusVariant(playbackInhibitsScreensaver)));
}

bool ScreensaverSettings::getAudioInhibitsScreensaver() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"audio_inhibits_screensaver");
	return reply.value().toBool();
}
void ScreensaverSettings::setAudioInhibitsScreensaver(
		bool inhibitsScreensaver) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "audio_inhibits_screensaver",
			QVariant::fromValue(QDBusVariant(inhibitsScreensaver)));
}

double ScreensaverSettings::getVolumeThreshold() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"audio_sleep_inhibit_power");
	return reply.value().toDouble();
}
void ScreensaverSettings::setVolumeThreshold(double volumeThreshold) {
	// volumeThreshod ranges from 0 (silent) to 1 (loud).
	double audioPower = volumeThreshold;
	audioPlayer->setVolume(audioPower);
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "audio_sleep_inhibit_power",
			QVariant::fromValue(QDBusVariant(audioPower)));
}

bool ScreensaverSettings::getNotificationsEnabled() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"notifications_enabled");
	return reply.value().toBool();
}
void ScreensaverSettings::setNotificationsEnabled(bool notificationsEnabled) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "notifications_enabled",
			QVariant::fromValue(QDBusVariant(notificationsEnabled)));
}

int ScreensaverSettings::getNotificationDuration() {
	QDBusReply<QVariant> reply = screensaverControl_properties->call(
			QDBus::CallMode::Block, "Get", "com.ScreensaverControl",
			"notification_duration");
	return reply.value().toInt();
}
void ScreensaverSettings::setNotificationDuration(int duration) {
	screensaverControl_properties->call(QDBus::CallMode::Block, "Set",
			"com.ScreensaverControl", "notification_duration",
			QVariant::fromValue(QDBusVariant(duration)));
}

void ScreensaverSettings::restoreDefaults() {
	screensaverControl->call(QDBus::CallMode::Block, "resetToDefaults");
}

void ScreensaverSettings::handlePauseMusicCheckBoxStateChange(int state) {
	setPauseForScreensaver(state != 0);
}

void ScreensaverSettings::handleResumeMusicCheckBoxStateChange(int state) {
	setResumeAfterScreensaver(state != 0);
}

void ScreensaverSettings::handleMuteVolumeCheckBoxStateChanged(int state) {
	setMuteForScreensaver(state != 0);
}

void ScreensaverSettings::handleUnmuteVolumeCheckboxStateChanged(int state) {
	setUnmuteAfterScreensaver(state != 0);
}

void ScreensaverSettings::handlePlaybackInhibitsCheckBoxStateChanged(
		int state) {
	setPlaybackInhibitsScreensaver(state != 0);
}

void ScreensaverSettings::handleAudioInhibitsCheckBoxStateChange(int state) {
	setAudioInhibitsScreensaver(state != 0);
}

void ScreensaverSettings::handleVolumeThresholdSliderValueChanged(
		int sliderPosition) {
	setVolumeThreshold(
			log1p(
					((double) sliderPosition) / VOLUME_THRESHOLD_SLIDER_MAX
							/ NUMFREQUENCIES));
	if (volumeThresholdSlider->hasFocus()) {
		audioPlayer->unpauseAsync();
		QTimer::singleShot(VOLUME_DURATION, this, SLOT(checkUnpauseAsync()));
	}
}

void ScreensaverSettings::handleNotificationEnabledCheckboxStateChanged(
		int state) {
	setNotificationsEnabled(state != 0);
}

void ScreensaverSettings::handleNotificationDurationSpinBoxValueChanged(
		int value) {
	setNotificationDuration(value * 1000);
}

void ScreensaverSettings::handleRestoreDefaultsButtonClicked(bool checked) {
	restoreDefaults();
}

void ScreensaverSettings::handleVolumeSliderPressed() {
	volumeSliderPressed = true;
	audioPlayer->unpauseAsync();
}

void ScreensaverSettings::handleVolumeSliderReleased() {
	volumeSliderPressed = false;
	audioPlayer->pauseAsync();
}

void ScreensaverSettings::checkUnpauseAsync() {
	if (!volumeSliderPressed) {
		audioPlayer->pauseAsync();
	}
}
