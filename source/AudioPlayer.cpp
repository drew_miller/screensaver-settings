#include "AudioPlayer.h"

#include <screensaver-control/Config.h>

#include "math.h"
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <unistd.h>

AudioPlayer::AudioPlayer(QObject *parent) :
		QObject(parent) {
	/* The Sample format to use */
	currVolume = SCREENSAVER_AUDIO_DEFAULT_SLEEP_INHIBIT_POWER;
	nextVolume = SCREENSAVER_AUDIO_DEFAULT_SLEEP_INHIBIT_POWER;
	s = NULL;
	mutex = new QMutex();
	currStateChangedCondition = new QWaitCondition();
	nextStateChangedCondition = new QWaitCondition();
	currState = stopped;
	nextState = stopped;
}

AudioPlayer::~AudioPlayer() {
	stop();
	delete currStateChangedCondition;
	delete nextStateChangedCondition;
	delete mutex;
}

void AudioPlayer::run() {
	// currState is now stopped.
	mutex->lock();
	currState = paused;
	nextState = paused;
	mutex->unlock();
	const pa_sample_spec ss = { PA_SAMPLE_S32NE, SAMPLERATE, 1 };
	int ret = 1;
	int error;
//	double frequencies[3] = {233.082, 293.665, 349.228};
	double frequencies[3] = { 261.626, 329.628, 391.995 };
	long offset = 0;
	if (!(s = pa_simple_new(NULL, "ScreensaverSettings", PA_STREAM_PLAYBACK,
			NULL, "TestSound", &ss, NULL, NULL, &error))) {
		std::cerr << "pa_simple_new() failed." << std::endl;
		goto finish;
	}
	double prebuf[BUFSIZE];
	int buf[BUFSIZE];
	mutex->lock();
	while (true) {
		if (nextState == paused) {
			currState = nextState;
			currStateChangedCondition->wakeAll();
			while (nextState == paused) {
				nextStateChangedCondition->wait(mutex);
			}
		}
		if (nextState == stopped) {
			if (currState != stopped) {
				currState = stopped;
				currStateChangedCondition->wakeAll();
			}
			break;
		}
		if (currState != nextState) {
			currState = nextState;
			currStateChangedCondition->wakeAll();
		}
		currVolume = nextVolume;
		while (nextState == playing) {
//			currVolume += (nextVolume - currVolume) * 0.10;
			currVolume += (nextVolume - currVolume) * BUFSIZE
					/ (SAMPLERATE + 0.0) * 10.0;
			mutex->unlock();
			double sum = 0;
			for (int i = 0; i < BUFSIZE; i++) {
				prebuf[i] = 0;
				for (int j = 0; j < NUMFREQUENCIES; j++) {
					prebuf[i] +=
							(sin(
									(i + offset) * 2 * PI
											* frequencies[j]/ SAMPLERATE));
				}
				buf[i] = (int) (prebuf[i] * currVolume * INT_MAX);
			}
			offset += BUFSIZE;
			// Generate a middle C sine wave.
			// Play the sine wave.
			if (pa_simple_write(s, buf, (size_t) (4 * BUFSIZE), &error) < 0) {
				fprintf(stderr, __FILE__": pa_simple_write() failed: %s\n",
						pa_strerror(error));
				goto finish;
			}
			mutex->lock();
		}
//		return ret;
//		mutex->lock();
	}
	/* Make sure that every single sample was played */
	if (pa_simple_drain(s, &error) < 0) {
		fprintf(stderr, __FILE__": pa_simple_drain() failed: %s\n",
				pa_strerror(error));
		goto finish;
	}
	ret = 0;
	finish: if (s) {
		pa_simple_free(s);
	}
	mutex->unlock();
}

bool AudioPlayer::pause() {
	mutex->lock();
	if (currState != paused) {
		if (nextState != paused) {
			nextState = paused;
			nextStateChangedCondition->wakeAll();
		}
		currStateChangedCondition->wait(mutex);
	}
	bool retValue = (currState == paused);
	mutex->unlock();
	return retValue;
}

void AudioPlayer::pauseAsync() {
	mutex->lock();
	if (nextState != paused) {
		nextState = paused;
		nextStateChangedCondition->wakeAll();
	}
	mutex->unlock();
}

bool AudioPlayer::unpause() {
	mutex->lock();
	if (currState != playing) {
		if (nextState != playing) {
			nextState = playing;
			nextStateChangedCondition->wakeAll();
		}
		currStateChangedCondition->wait(mutex);
	}
	bool retValue = (currState == playing);
	mutex->unlock();
	return retValue;
}

void AudioPlayer::unpauseAsync() {
	mutex->lock();
	nextState = playing;
	nextStateChangedCondition->wakeAll();
	mutex->unlock();
}

bool AudioPlayer::stop() {
	mutex->lock();
	while (currState != stopped) {
		if (nextState != stopped) {
			nextState = stopped;
			nextStateChangedCondition->wakeAll();
		}
		currStateChangedCondition->wait(mutex);
	}
	bool retValue = (currState == stopped);
	mutex->unlock();
	return retValue;
}

void AudioPlayer::stopAsync() {
	mutex->lock();
	if (nextState != stopped) {
		nextState = stopped;
		nextStateChangedCondition->wakeAll();
	}
	mutex->unlock();
}

bool AudioPlayer::isPlaying() {
	return currState == playing;
}

bool AudioPlayer::isPaused() {
	return currState == paused;
}

bool AudioPlayer::isStopped() {
	return currState == stopped;
}

double AudioPlayer::getVolume() {
	return nextVolume;
}

void AudioPlayer::setVolume(double volume) {
	this->nextVolume = volume;
}
