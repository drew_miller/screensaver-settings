#!/bin/bash
set -e

if [ $1 == "--all" -o $1 == "--debug" ]
then
	mkdir -p debug
	pushd debug
#	rm -r *
	cmake -GNinja -DCMAKE_BUILD_TYPE=DEBUG ../source
	ninja
	popd
elif [ $1 == "--all" -o $1 == "--release" ]
then
	mkdir -p release
	pushd release
#	rm -r *
	cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE ../source
	ninja
	popd
elif [ $1 == "--all" -o $1 == "--package" ]
then
	mkdir -p package
	pushd package
#	rm -r *
	cmake ../source -DCMAKE_BUILD_TYPE=PACKAGE
	popd
else
	echo "Build type $1 not defined."
fi
